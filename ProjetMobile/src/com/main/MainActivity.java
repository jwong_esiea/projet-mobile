package com.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

/**
 * First activity
 * Main menu Activity
 * @author James
 * 
 */
public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	/**
	 * Select a new Menu
	 * 
	 * @param view
	 */
	public void selectedMenu(View view) {
		Intent intent;
		//get the button with one we have click
		switch (view.getId()) {
		case R.id.buttonPlay:
			intent = new Intent(this, Playmode.class);
			break;
		case R.id.buttonScore:
			intent = new Intent(this, ScoreView.class);
			break;
		default:
			intent = this.getIntent();
			break;
		}
		startActivity(intent);

	}
}
