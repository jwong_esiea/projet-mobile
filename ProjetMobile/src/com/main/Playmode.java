package com.main;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * @author James
 * 
 */
public class Playmode extends Activity implements SensorEventListener {
	// this class are the play view for the users, in this class we initialize a
	// timer at 30 seconds
	// and we count the number of movement superior at 10 g.
	private final static float GRAVITY = 10;
	private final static float GRAVITYNEG = -10;
	private final static Class nextActivity = Result.class;
	SensorManager sensor;
	Sensor accelerometer;
	int count = 0;

	private View addListenerToComponent(Animation animation, View view,
			View nextView) {
		View nextImage = null;
		switch (view.getId()) {
		case R.id.imageView1:
			nextImage = nextView;
			break;
		}
		return nextImage;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Hide the actionBar to get all the screen
		ActionBar actionBar = getActionBar();
		actionBar.hide();
		setContentView(R.layout.playview);
	}

	/**
	 * Send the score to the activity Result
	 */
	private void passToResult() {
		Intent nextIntent = new Intent(this, nextActivity);
		nextIntent.putExtra("parameterCount", count);
		startActivity(nextIntent);
	}

	/**
	 * On the start, we initialize all the accelerometer
	 */
	@Override
	protected void onStart() {
		super.onStart();
		final TextView counterDownField = (TextView) findViewById(R.id.chronometer_30sec); // initialize
		// chrono
		sensor = (SensorManager) getSystemService(SENSOR_SERVICE);
		accelerometer = sensor.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		ImageView imageView_timer_3 = (ImageView) findViewById(R.id.timer_3);
		final ImageView imageView_timer_2 = (ImageView) findViewById(R.id.timer_2);
		final ImageView imageView_timer_1 = (ImageView) findViewById(R.id.timer_1);
		final Animation animation_timer_2 = AnimationUtils.loadAnimation(this,
				R.animator.fade_in);
		final Animation animation_timer_1 = AnimationUtils.loadAnimation(this,
				R.animator.fade_in);
		final Animation animation_timer_0 = AnimationUtils.loadAnimation(this,
				R.animator.fade_in);

		// Set a countdown begin at 30 second
		final CountDownTimer countdown = new CountDownTimer(10000, 1000) {
			public void onTick(long millisUntilFinished) {
				counterDownField.setText(String
						.valueOf(millisUntilFinished / 1000));
				Log.d("Tag:CounterDown",
						String.valueOf(millisUntilFinished / 1000));
			}

			public void onFinish() {
				counterDownField.setText("Finish");
				passToResult();
			}
		};

		animation_timer_1.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {

			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationEnd(Animation animation) {

				imageView_timer_1.setAnimation(animation_timer_0);
				imageView_timer_1.startAnimation(animation_timer_0);
				countdown.start();

			}
		});

		animation_timer_2.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {

			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationEnd(Animation animation) {

				imageView_timer_2.setAnimation(animation_timer_1);
				imageView_timer_2.startAnimation(animation_timer_1);

			}
		});

		try {
			// Count down animation
			imageView_timer_3.setAnimation(animation_timer_2);
			imageView_timer_3.startAnimation(animation_timer_2);
		} catch (Exception e) {
			Log.e("Error Animation", e.toString());
		}

		ImageView man_start = (ImageView) findViewById(R.id.man_start);
		man_start.setVisibility(View.VISIBLE);

	}

	// When the application is running,we put a listener on the accelerometer to
	// get his information
	@Override
	protected void onResume() {

		sensor.registerListener(this, accelerometer,
				SensorManager.SENSOR_DELAY_UI);
		super.onResume();
	}

	// This stop the accelerometer,we quit the application
	@Override
	protected void onPause() {
		sensor.unregisterListener(this, accelerometer);
		super.onPause();
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub

	}

	/**
	 * Take the sensor in xGravity add the score if the gravity is superior than
	 * the Gravity
	 * 
	 * @param xGravity
	 */
	private void shakeEvent(float xGravity) {
		ImageView imageView_man_can_down = (ImageView) findViewById(R.id.man_can_down);
		ImageView imageView_man_can_up = (ImageView) findViewById(R.id.man_can_up);
		ImageView imageView_man_can_start = (ImageView) findViewById(R.id.man_start);
		if ((xGravity > GRAVITY) || (xGravity < GRAVITYNEG)) {
			count++;
			if (xGravity < GRAVITYNEG) {
				// when the user check to up the smartphone, we display the
				// picture with the character with the can up

				imageView_man_can_start.setVisibility(View.INVISIBLE);
				imageView_man_can_down.setVisibility(View.INVISIBLE);
				imageView_man_can_up.setVisibility(View.VISIBLE);
			}
			if (xGravity > GRAVITY) {
				// when the user check to down the smartphone, we display the
				// picture with the character with the can down
				imageView_man_can_start.setVisibility(View.INVISIBLE);
				imageView_man_can_down.setVisibility(View.VISIBLE);
				imageView_man_can_up.setVisibility(View.INVISIBLE);

			}

		}
	}

	/**
	 * This methods is activated when the accelerometer is using He get all the
	 * register data an use it
	 **/
	@Override
	public void onSensorChanged(SensorEvent event) {
		// we initialize the ImageView with the different id

		float xGravity = 0, yGravity = 0, zGravity = 0;

		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
			// we get the value of x , y , z position from the ACCELEROMETER
			xGravity = event.values[0];
			yGravity = event.values[1];
			zGravity = event.values[2];
		}
		shakeEvent(xGravity);
	}

}
