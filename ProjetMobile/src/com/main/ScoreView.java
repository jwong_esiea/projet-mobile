package com.main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.SimpleAdapter;

/**
 * @author James & Vedovato
 * 
 */

public class ScoreView extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.scoreview);
	}

	@Override
	protected void onStart() {
		super.onStart();
		ListView scoreListView = (ListView) findViewById(R.id.listView_score);
		SimpleAdapter simpleAdapter = new SimpleAdapter(this,this.read_data(),R.layout.listcontent, new String[]{"score"},new int[]{R.id.scoreList});
		scoreListView.setAdapter(simpleAdapter);


	}

	protected List<HashMap<String, score_data>> read_data() {
		List<HashMap<String, score_data>> data_stored = new ArrayList<HashMap<String, score_data>>();

		BufferedReader in = null;
		try {
			File scoreFile = new File(getFilesDir(), "score_data.txt");
			FileInputStream fileIS = new FileInputStream(scoreFile);
			InputStream inputStream = (InputStream) fileIS;
			in = new BufferedReader(new InputStreamReader(inputStream));
			String line;
			while ((line = in.readLine()) != null) {
				String[] rowdata = line.split(":");
				HashMap<String,score_data> hasmtmp = new HashMap<String, score_data>();
				hasmtmp.put("score", new score_data(rowdata[0], rowdata[1]));
				data_stored.add(hasmtmp);
			}
			in.close();
		} catch (final IOException e) {
			Log.e("ERROR reader", "ERROR reader");
		}

		return data_stored;
	}
}
