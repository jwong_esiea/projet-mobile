package com.main;

/**
 * Created by vedovato on 19/09/13.
 */
public class score_data {

	public String pseudo = "";
	public String score = "0";
	public int ranked = 0;

	public score_data(String pseudo, String score, int ranked) {
		this.pseudo = pseudo;
		this.score = score;
		this.ranked = ranked;
	}

	public score_data(String pseudo, String score) {
		this.pseudo = pseudo;
		this.score = score;
	}

	public String getPseudo() {
		return pseudo;
	}

	public String getScore() {
		return score;
	}

	public int getRanked() {
		return ranked;
	}

	public String toString() {
		return "Pseudo : " + pseudo + " Score :" + score;

	}

}
