package com.main;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Action for the result activity Created by vedovato on 17/09/13.
 */
public class Result extends Activity {
	final static String parameterCount = "parameterCount";
	int score = 0;

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle currentBundle = getIntent().getExtras();
		score = (Integer) currentBundle.getInt(parameterCount);
		setContentView(R.layout.result);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onStart()
	 */
	@Override
	protected void onStart() {
		super.onStart();
		TextView scoreTextView = (TextView) findViewById(R.id.textView_score);
		scoreTextView.setText(String.valueOf(score));

	}

	/**
	 * 
	 */
	public void save() {
	}

	/**
	 * Method let the user to return to the main menu
	 * 
	 * @param view
	 */
	public void returnToMainMenu(View view) {
		Intent intentToMainMenu = new Intent(this, MainActivity.class);

		// get the data wrote in the EditText
		String pseudo;
		EditText EditText_pseudo = (EditText) findViewById(R.id.editText_pseudo);
		pseudo = EditText_pseudo.getText().toString();

		// write the data in a simple format
		String data_save = pseudo +":"+ score + "\n";

		// save the path
		File scoreFile = new File(getFilesDir(), "score_data.txt");
		try {
			// create a save file
			FileWriter fileWriter = new FileWriter(scoreFile,true);
			fileWriter.write(data_save);
			fileWriter.close();
		} catch (Exception e) {
			Log.e("Exception", "File write failed: " + e.toString());
		}
		startActivity(intentToMainMenu);
	}

	/**
	 * @param count
	 */
	public void count_result(float count) {
		// use this class to calculate the result from the previous class
		// " play mode "
		// we receive a float named count this variable will be use a simple
		// calcul to
		// anim the animation on the result view and change the "label altitude"

		int size_width = findViewById(R.layout.result).getWidth();
		int size_height = findViewById(R.layout.result).getHeight();

		float size = count / 4;
		float start_X = 500;
		Canvas canvas = new Canvas();

		int width = canvas.getWidth();
		int height = canvas.getHeight();

		Paint paint = new Paint();
		paint.setARGB(100, 0, 0, 255);// creat a blue color
		paint.setStrokeWidth(30);// define the size of the futur pointer

		// canvas.drawLine();
	}

}
